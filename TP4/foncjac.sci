function [F,J]= foncjac(x)
    
    if (length(x) <> 2) then
        error("Error in foncjac: X is not an R² vector")
    end
    
    /* Foncjac: préparation
    F = [(x(1) - 1)^2 + x(2)^2 - 4 ; x(2) - x(1)^2]
    
    J = [ (2*x(1) - 2) (2*x(2));(-2*x(1)) 1 ]
    
    */ 
    
    // Foncjac: question 2
    
    F = [ x(1) + x(2)^2 - 4; x(2) - exp(x(1))]
    
    J = [ 2*x(1) 2*x(2); -exp(x(1)) 1]
    
endfunction
