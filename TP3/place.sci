function [i] = place(T,t)
    
    [n,m] = size(T);
    if (m <> 1) then
        error("Error in place function, T is not a vector");
    end
    
    if (t < T(1) || t > T(n)) then
        error("Error in place.sce : given parameter is not between T1 and Tn")
    end
    
    //Variable declaration
    imin  = 1;
    imax = n;
    
    while ((imax - imin) > 1 ) then
        mil = floor((imax + imin) /2 );
        if (t >= T(mil)) then
            imin = mil;
        else
            imax = mil;
        end
    end
    
    i = imin;
    
endfunction
