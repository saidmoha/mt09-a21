exec place.sci

function trace(N,T,cc)
    //Get matrix sizes
    [n,m] = size(T);
    [ncc,mcc] = size(cc);
    
    //Check validity
    if (m <> 1) then
        error("Error in place function, T is not a vector");
    end
    if ( (ncc <> (n-1)) || (mcc <> 3)) then
        error("Errpr in place function, N matrix is not a (n-1) x 3 matrix");
    end
    
    //Program starts here
    
    //Local variables
    tabGT = zeros(1,N);
    tabT = linspace(T(1),T(n),N);

    for j=1:N
        i = place(T,tabT(j));
        t = T(i);
        tabGT(j) = cc(i,1) + cc(i,2) * tabT(j) + cc(i,3) * tabT(j)^2;
        
    end
    
    plot(tabT, tabGT);

endfunction
