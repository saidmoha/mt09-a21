/*
n vaut le degré du polynome que l'on recherche. 
Nous recherchons un polynome de la forme:

p(t) = x0 + x1*t + x2*t^2

Ainsi n = 3
*/

function [A] = constrpoly(tau)
    
    m = length(tau);
    A = [ones(m,1) tau tau^2];

endfunction
