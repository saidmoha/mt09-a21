function [x] = mcnorm(A,y)
    [m,n] = size(A);
    
    if (length(y) <> m) then
        error("Error in mcnorm: y does not match A dimensions");
    end
    
    x = (A' * A) \(A' * y);
endfunction
