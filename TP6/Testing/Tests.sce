//Préparation
exec('constrpoly.sci', -1)
exec('mcnorm.sci', -1)


tau = [0;1;2]
y = [2;-3;5]
A = constrpoly(tau)
x = mcnorm(A, y)
Ptau = A*x

//b
//tracer la parabole obtenue ainsi que les points de coordonn ˆ ees
t=-3:0.1:5
plot(t,constrpoly(t')*x)
plot(tau,y,'ro')


//c
tau1 = (0:0.25:2)'
y1 = [-1;-0.7;0.95;1.8;3;2.95;1.45;0.9;-0.6]
A0 = constrpoly(tau1)
x1 = mcnorm(A0, y1)
t1=-2:0.05:4
scf(1)
plot(t1,constrpoly(t1')*x1)
plot(tau1,y1,'ro')



//TP6
//1.c
exec('construct.sci', -1)
tc = [0; 1 ; 3 ; 4 ; 5.5 ; 6] 
tauc = tc
A1 =  construct(tc,tauc)

//1.d
td = [0; 1; 3; 4; 5.5; 6]
taud = [0.5; 2; 2.5; 3.5; 4.5; 5.75]
A2 =  construct(td,taud)

//1.e
te = [0; 1; 3; 4; 5.5; 6]
taue=(0:0.3:6)'
A3 =  construct(te,taue)


//2
exec('solinf.sci', -1)
exec('solsup.sci', -1)
exec('cholesky.sci', -1)
exec('resolchol.sci', -1)
exec('mcnormbis.sci', -1)

//2.b
td = [0; 1; 3; 4; 5.5; 6]
taud = [0.5; 2; 2.5; 3.5; 4.5; 5.75]
A2 =  construct(td,taud)
y = [1; 1.5; 1.25; 0; 0; 1.5]
z = mcnormbis(A2, y)
fz=A2*z

n =  size(td)(1)
scf(2)
for i=1:n-1
    points = linspace(td(i),td(i+1), 20)
    plot(points,construct(td,points')*z) 
end
plot(taud,y,'ro')


//2.c
te = [0; 1; 3; 4; 5.5; 6]
taue=(0:0.3:6)'
A3 =  construct(te,taue)
y = [0; 0.6; 1.4; 1.7; 2.1; 1.9; 1.6; 1.4; 1.4; 1; 0.5; 0.4; -0.2; -0.8; -0.5; 0; 0.4; 1; 1.6; 1.7; 1.2]
z1 = mcnormbis(A3, y)
fz1=A3*z1

n =  size(te)(1)
scf(3)
for i=1:n-1
    points = linspace(te(i),te(i+1), 20)
    plot(points,construct(te,points')*z1) 
end
plot(taue,y,'ro')

condM=cond((A3')*A3)


