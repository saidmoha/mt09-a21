exec "solsup.sce"
function z=mcQR(A,y)
    [n,m]=size(A);
    [Q,R]=qr(A);
    b=Q'*y;
    r = R(1:m,1:m);
    disp(cond(r));
    c = b(1:m);
    z=solsup(r,c);
endfunction
