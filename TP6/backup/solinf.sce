function[x] = solinf(L,b)
    //Fonction qui résoud le système Lx = b
    [nL,mL] = size(L);
    [nB,mB] = size(b);
    
    //Check validity of matricial product
    if (mL <> nB || mB <> 1) then
        error("erreur dans solinf, dimensions non conformes !");
    end
    
    //Execution of the program
    x = zeros(nB,1);
    
    for i=1:nB
        
        //Error if pivot is equal to 0
        if abs(L(i,i)) <= %eps
            error("erreur dans solinf, pivot nul");
        end
        
        x(i,1) =  1/L(i,i);
        c = b(i,1);
        for j=1:i-1
            c = c - L(i,j) * x(j,1);
        end
        x(i,1) = x(i,1) * c;
    end
    
endfunction
    
    
function[x] = solinf2(L,b)
    //Fonction qui résoud le système Lx = b
    [nL,mL] = size(L);
    [nB,mB] = size(b);
    
    //Check validity of matricial product
    if (mL <> nB || mB <> 1) then
        error("erreur dans solinf, dimensions non conformes !");
    end
    
    //Execution of the program
    x = zeros(nB,1);
    
    for i=1:nB
        
        //Error if pivot is equal to 0
        if abs(L(i,i)) <= %eps
            error("erreur dans solinf, pivot nul");
        end
        
        x(i,1) =  1/L(i,i);
        /*for j=1:i-1
            c = c - L(i,j) * x(j,1);
        end*/
        c = b(i,1) - L(i, 1:i-1)*x(1:i-1,1)
        x(i,1) = x(i,1) * c;
    end
    
endfunction


function [x] = solinf3(L, b)
    [n m] = size(L)
    x = zeros(n,1)
    for i=1:n
        if L(i, i) == 0
            error("Matrice non inversible")
        end
        s = 0
        /*for j = n:-1:i
            s = s + U(i, j) * x(j)
        end*/
        s = s + L(i, 1:i-1) * x(1:i-1,1)
        x(i) = (b(i) - s) / L(i, i)
    end
endfunction
