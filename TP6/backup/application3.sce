exec 'mcQR.sci'
exec 'construct.sci'
exec 'mcnorm.sci'


te = [0; 1; 3; 4; 5.5; 6];
taue=(0:0.3:6)';
A3 =  construct(te,taue);
y = [0; 0.6; 1.4; 1.7; 2.1; 1.9; 1.6; 1.4; 1.4; 1; 0.5; 0.4; -0.2; -0.8; -0.5; 0; 0.4; 1; 1.6; 1.7; 1.2];

z = mcQR(A3,y);


n =  size(te)(1)
for i=1:n-1
    points = linspace(te(i),te(i+1), 20)
    plot(points,construct(te,points')*z) 
end
plot(taue,y,'ro')



