exec "../TP2/solinf.sce";
exec "../TP2/solsup.sce"
exec "cholesky.sci"
function [x] = resolchol(A, b)
    C = cholesky(A);
    y = solinf(C,b);
    x = solsup(C',y);
endfunction
