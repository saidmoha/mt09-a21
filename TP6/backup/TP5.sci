//QUESTION 1
function [f] = diffdiv(y,t)
    n = length(t)
    d = zeros(n,1)
    f = zeros(n,1)
    f = y
    if (n <> length(y))
        error('Dimensions incompatibles') 
    end
    for i = 1 : n
        d(i) = y(i)
    end
    for k = 2 : n
        for i = n : -1 : k
            d(i) = (d(i) - d(i-1))/(t(i) - t(i-k+1))
            f(i) = d(i);
        end
    end
endfunction

//Application
//[f1] = diffdiv(y,t)

//QUESTION 2
function [p,pprim] = horn(a,t,theta)
    
    disp(a)
    disp(t)
    n = length(a);
    
    if (length(t)<> n-1)
        error('Dimensions des vecteurs a et t incompatibles')
    end
    
    b = zeros(n,1);
    b($) = a(n);
    
    for k = n-1 : -1 : 1
        b(k) = a(k)-b(k+1)*(t(k)-theta)
    end
    
    p = b(1)
    c = zeros(n-1,1)
    c($) = b(n)
    for k = n-2 : -1 : 1
        c(k) = b(k+1) - c(k+1)*(t(k)-theta)
    end
    pprim = c(1);
    
endfunction

//Applications
//[p1,pprim1] = horn(a,t2,2)

function [p] = Holmer(a,t,theta)
        n = length(a)
        if (length(t) <> n-1)
            error('Dimensions incohérentes entre a et t') 
        end
        p = a($)
        for k = n-1 : -1 : 1
            p = a(k) + (theta - t(k))*p
        end
endfunction

//Holmer(a,t2,2)

function [p] = Holmerv(a,t,theta)
    m = length(theta);
    p = zeros(m,1);
    for k = 1 : m
        p(k) = Holmer(a,t,theta(k))
    end
endfunction

//Holmerv(a,t2,[2;4])

//Question3
function [p] = interpole(y,t,theta)
        n = length(t)
        if (length(y) <> n)
            error('Dimensions incohérentes entre y et t') 
        end
        f = diffdiv(y,t)
        c = f;
        [p] = Holmerv(c,t(1:n-1),theta)
        
endfunction

//interpole(y,t,3)
//interpole(y,t,4)
//interpole(y,t,3)

function [p,pprim] = interpol_horn(y,t,theta)
        n = length(t)
        if (length(y) <> n)
            error('Dimensions incohérentes entre y et t') 
        end
        f = diffdiv(y,t)
        c = f;
        [p,pprim] = horn(c,t(1:n-1),theta)
endfunction


function courbe8(y,t,N,tt)
    scf(0)
    clf
    theta = linspace(t(1),t($),N)
    p = zeros(N,1)
    p = interpole(y,t,theta)
    plot(t,y,'og')
    plot(theta',p)
    
    //Traitement pour la tangente
    [ptan,pprimtan] = interpol_horn(y,t,tt)
    plot(t,pprimtan*(t-tt)+ptan,'r')
    
endfunction

//courbe1(y,t,200,4)

function [g] = interpol_spline(y,t,theta)
    exec("C:\Users\alban\OneDrive\Bureau\GI01\MT09\TP\TP5\TP3.sce",-1)
    cc = calcoef(t,y);
    g = calcg_horn(theta,t,cc)
endfunction

function [g] = calcg_horn(t,T,cc)
    n = length(T)
    i = place(T,t)
    g = cc(i,3) + cc(i,4)*(t - T(i+1));
    
    for k = 2 : -1 : 1
        g = cc(i,k) + (t - T(i))*g
    end 
    
endfunction

function [y] = calcg(t,T,cc)
    i = place(T,t);
    disp(i)
    y = cc(i,1)+ cc(i,2)*(t - T(i)) + cc(i,3)*(t - T(i))^2 + cc(i,4)*(t - T(i))^2*(t-T(i+1)); 
    
endfunction

function courbe_gP(t,y,N)
    n = length(t);
    
    if (length(y) <> n) then
        error('Dimensions incompatibles')
    end
    
    p = zeros(n,1);
    g = zeros(n,1)
    
    theta = linspace(t(1),t($),N)
    
    p = interpole(y,t,theta);
    
    for k = 1 : N  
        g(k) = interpol_spline(y,t,theta(k))
    end
    scf(1)
    clf
    plot(t,y,'og')
    plot(theta',p,'b')
    plot(theta',g,'r')
    
endfunction

test = [1, 3, 4.5, 5, 6]';
ytest = [1, 5, 3, 7, -1]';
t4 = linspace(0,1,10)
