exec constrpoly.sci
exec mcnorm.sci

n=100

//Application 1 :


tau = [0; 1; 2];
y = [2; -3; 5];

A = constrpoly(tau);
x = mcnorm(A, y);

disp(x);

plot(tau, y, 'bo');

/*
g = A*x;

plot(τ, g, 'r--');
*/

t=linspace(-1, 3, n);
p=zeros(n, 1);

for i=1:n
    p(i) = x(1) + x(2)*t(i) + x(3)*t(i)^2 ;
end

plot(t, p, 'r--');

