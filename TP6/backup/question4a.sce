exec 'mcQR.sci'
exec 'construct.sci'
exec 'mcnorm.sci'
exec 'TP5.sci'

N = 200;

td = [0; 1; 3; 4; 5.5; 6]
taud = [0.5; 2; 2.5; 3.5; 4.5; 5.75]
A2 =  construct(td,taud)
y = [1; 1.5; 1.25; 0; 0; 1.5]
z = mcnormbis(A2, y)
fz=A2*z

n =  size(td)(1)
scf(2)
for i=1:n-1
    points = linspace(td(i),td(i+1), N)
    plot(points,construct(td,points')*z) 
end
plot(taud,y,'ro')

courbe(y,taud,N,td)


