function [C] = cholesky(a)
    [na, ma] = size(a);
    
    C = zeros(na,ma);

    if (a(1,1) < %eps) then
        error("Error in Cholesky, element is null");
    end
    
    C(1,1) = sqrt(a(1,1));
    for j = 2 : na
        C(j,1) = a(j,1) / C(1,1);
    end
    for i = 2 : na
        somme = 0;
        somme = somme + (C(i,1:i-1)) * (C(i,1:i-1))';
        
        if ((a(i,i) -somme) < %eps) then
            error("Error in Cholesky, sqrt not possible");
        end
        C(i,i) = sqrt(a(i,i) - somme);
        
        for j = i+1 : na
            somme = 0;
            somme = somme + C(j,1:i-1)*C(i,1:i-1)';
            if (C(i,i) < %eps) then
                error("Error in Cholesky, division by 0");
            end
            C(j,i) = (a(j,i) - somme)/C(i,i);
        end 
    end

    
endfunction
