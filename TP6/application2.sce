//Application 2
exec constrpoly.sci
exec mcnorm.sci

n = 100;

figure(2);

tau = linspace(0,2,9)';
y = [-1, -0.7, 0.95, 1.8, 3., 2.95, 1.45, 0.9, -0.6]';

A = constrpoly(tau);
x = mcnorm(A, y);

disp(x);

plot(tau, y, 'bo');

t=linspace(-1, 3, n)';
p=zeros(n, 1);

for i=1:n
    p(i) = x(1) + x(2)*t(i) + x(3)*t(i)^2 ;
end


plot(t, p, 'r--');
