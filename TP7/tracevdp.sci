exec pointmilieu.sci
exec vdp.sci
exec eulerexpl.sci
exec RK4.sci

function tracevdp(a, t0, T, Neul, Nptmil, Node, NRK4)

    t = linspace(t0,T+t0,Nptmil+1);
    z = pointmilieu(a,t0,T,Nptmil,vdp);
    z2 = eulerexpl(a,t0,T,Neul,vdp);
    z3 = ode(a, t0, t, vdp);
    z4 = RK4(a,t0,T,NRK4,vdp);
    scf(0)
    clf
    disp(size(z4));
    plot(t,z,t,z2,t, z3, t, z4);
    
endfunction
