function Y = vdp(t, X)
    Y = zeros(2,1);
    c = 0.4;
    
    Y(1) = X(2);
    Y(2) = c*(1 - X(1)^2)*X(2) - X(1);
endfunction
