exec eulerexpl.sci
exec funexpt2.sci
exec pointmilieu.sci


a = 0;
t0 = 0;
T = 5;
N = 50;

Z = eulerexpl(a,t0,T,N,funexpt2);
//Z = pointmilieu(a,t0,T,N,funexpt2);

//Affichage
figure(1);

t = linspace(t0,t0+T,N);
y = zeros(1,N);
for i=1:N
    y(i) = 2*exp(t(i)) - t(i)^2 - 2*t(i) - 2
end
clf

plot(t,y,'r--');
plot(t,Z(1:N),'b--');


