function Z = RK4(a, t0, T, N, f)
    //Schéma de RK4
    
    //Définition de Z
    p = length(a);
    Z = zeros(p, N+1);
    if (N == 0) then
        error("Division by 0");
    end
    h = T/N;
    
    Z(:,1) = a;
    
    for i=2:N
        K0 = f ( (t0 + (i-1)*h), Z(:,i-1) );
        K1 = f ( (t0 + (i-1)*h) + h/2, Z(:,i-1) + (h/2)*K0 );
        K2 = f ( (t0 + (i-1)*h) + h/2, Z(:,i-1) + (h/2)*K1 );
        K3 = f ( (t0 + (i-1)*h) + h, Z(:,i-1) + h*K2 );
        Z(:,i) = Z(:,i-1) + (h/6)* (K0 + 2*K1 + 2*K2 + K3);
    end
endfunction
