function [v] = funexpt2(theta, u)
    v = u + theta * theta;
endfunction
