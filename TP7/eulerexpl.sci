function [Z] = eulerexpl(a, t0, T, N, f)
    //Définition de Z
    p = length(a);
    Z = zeros(p, N+1);
    if (N == 0) then
        error("Division by 0");
    end
    h = T/N;
    
    Z(:,1) = a;
    for i=2:N
        Z(:,i) = Z(:,i-1) + h * f( (t0 + (i-1)*h) , Z(:,i-1) );
    end
endfunction
