function Z = pointmilieu(a, t0, T, N, f)
    //Schéma Point du milieu
    
    //Définition de Z
    p = length(a);
    Z = zeros(p, N+1);
    if (N == 0) then
        error("Division by 0");
    end
    h = T/N;
    
    Z(:,1) = a;
    for i=2:N
        yndemi = Z(:,i-1) + (h/2) *  f( (t0 + (i-1)*h), Z(:,i-1) );
        Z(:,i) = Z(:,i-1) + h * f( (t0 + (i-1)*h) + h/2 , yndemi);
    end
endfunction
