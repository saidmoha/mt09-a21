U = [[1 2 3];[0 4 8];[0 0 5]]
b = [[6];[16];[15]]

function [x] = solsup(U, b)
    [n m] = size(U)
    x = zeros(n,1)
    for i=n:-1:1
        if U(i, i) == 0
            error("Matrice non inversible")
        end
        s = 0
        for j = n:-1:i
            s = s + U(i, j) * x(j)
        end
        x(i) = (b(i) - s) / U(i, i)
    end
endfunction

