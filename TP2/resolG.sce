exec ../TP2/trisup.sce
exec ../TP2/solsup.sce
function [x] = resolG(A, b)
    [U, e] = trisup(A, b)
    [x] = solsup(U, e)
endfunction
