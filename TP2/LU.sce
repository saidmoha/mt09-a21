function [L, U] = LU(A, b)
    [n m] = size(A)
    tol = 1.d-16
    L = eye(n,n)
    U = zeros(n,n)
    for k=1:n-1
        if abs(A(k, k)) < tol
            error("erreur")
        else
            for i = k+1:n
                L(i, k) = A(i, k) / A(k, k)
                b(i) = b(i) - L(i, k)*b(k)
                A(i, k) = 0
                for j = k+1:n
                    A(i, j) = A(i, j) - L(i, k)*A(k, j)
                end
            end
        end
    end
    if abs(A(n, n)) < tol
        error("erreur")
    end
    [U] = [A]
endfunction
