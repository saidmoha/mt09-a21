function [U, e] = trisup(A, b)
    [n m] = size(A)
    tol = 1.d-16
    for k=1:n-1
        if abs(A(k, k)) < tol
            error("Error: pivot nul")
        else
            for i = k+1:n
                c = A(i, k) / A(k, k)
                b(i) = b(i) - c*b(k)
                A(i, k) = 0
                for j = k+1:n
                    A(i, j) = A(i, j) - c*A(k, j)
                end
            end
        end
    end
    if abs(A(n, n)) < tol
        error("Error in trisup: pivot nul, matrice non inversible")
    end
    [U, e] = [A, b]
endfunction
