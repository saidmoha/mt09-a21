function[x] = solinf(L,b)
    //Fonction qui résoud le système Lx = b
    [nL,mL] = size(L);
    [nB,mB] = size(b);
    
    //Check validity of matricial product
    if (mL <> nB || mB <> 1) then
        error("erreur dans solinf, dimensions non conformes !");
    end
    
    //Execution of the program
    x = zeros(nB,1);
    
    for i=1:nB
        
        //Error if pivot is equal to 0
        if L(i,i) <= %eps;
            error("erreur dans solinf, pivot nul");
        end
        
        x(i,1) =  1/L(i,i);
        c = b(i,1);
        for j=1:i-1
            c = c - L(i,j) * x(j,1);
        end
        x(i,1) = x(i,1) * c;
    end
    
endfunction
    
